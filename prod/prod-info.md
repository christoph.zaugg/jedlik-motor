# Production documents jedlik-motor

This folder contains gerber data of the jedlik-motor-side and jedlik-motor-base.

PCB must be 1.6mm thickness, color and surface finish can be chosen without restrictions. 

## Remarks

Generated documents in repository's are not optimal. As long as there is now good way to generate this documentation with CI/CD pipelines, it is better to manually generate it and put them into this folder. Try the best to keep it up to date.

***(c)2022 by shasta***
