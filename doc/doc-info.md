# Documentation of the jedlik-motor

This folder contains the PCB drawings for jedlink-motor-base and jedlink-motor-side.

## Remarks

PDF documents in repository's are not optimal. As long as there is now good way to generate this documentation with CI/CD pipelines, it is better to manually generate pdfs and put them into this folder. This way people don't need to download KiCad and clone the repo to look at the design. Try the best to keep it up to date.

***(c)2022 by shasta***
